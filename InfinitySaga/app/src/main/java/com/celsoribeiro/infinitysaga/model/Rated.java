package com.celsoribeiro.infinitysaga.model;

import java.io.IOException;
import java.io.Serializable;

public enum Rated implements Serializable {
    PG_13;

    public String toValue() {
        switch (this) {
            case PG_13: return "PG-13";
        }
        return null;
    }

    public static Rated forValue(String value) throws IOException {
        if (value.equals("PG-13")) return PG_13;
        throw new IOException("Cannot deserialize Rated");
    }
}