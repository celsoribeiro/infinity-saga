package com.celsoribeiro.infinitysaga.data;

import com.celsoribeiro.infinitysaga.model.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface InfinitySagaApi {

    @GET("saga")
    Call<List<Movie>> getAllMovies();
    
}