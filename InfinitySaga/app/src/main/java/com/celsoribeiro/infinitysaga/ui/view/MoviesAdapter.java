package com.celsoribeiro.infinitysaga.ui.view;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.infinitysaga.R;
import com.celsoribeiro.infinitysaga.model.Movie;
import com.celsoribeiro.infinitysaga.ui.presenter.MoviesPresenter;

import java.util.List;

class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


  private final List<Movie> movieList;
  private final MoviesPresenter presenter;

  MoviesAdapter(List<Movie> movieList, MoviesPresenter presenter) {
    this.movieList = movieList;
    this.presenter = presenter;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_row, parent, false);
    return new MovieViewHolder(view,presenter);
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    MovieViewHolder heroViewHolder = (MovieViewHolder) holder;
    Movie movie = movieList.get(position);
    heroViewHolder.render(movie);

  }

  @Override public int getItemCount() {
    return movieList.size();
  }
}
