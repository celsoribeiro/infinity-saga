package com.celsoribeiro.infinitysaga.data;

import com.celsoribeiro.infinitysaga.data.response.ResponseListener;
import com.celsoribeiro.infinitysaga.model.Movie;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class InfinitySagaRemoteRepository {

    private InfinitySagaApi infinitySagaApi;

    public InfinitySagaRemoteRepository() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://private-b34167-rvmarvel.apiary-mock.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        infinitySagaApi = retrofit.create(InfinitySagaApi.class);

    }

    public void getAllMovies(final ResponseListener listener) {
        infinitySagaApi.getAllMovies().enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                listener.onFailure();
            }
        });

    }


}
