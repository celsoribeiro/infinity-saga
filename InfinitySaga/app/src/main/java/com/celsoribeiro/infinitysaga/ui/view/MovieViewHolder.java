package com.celsoribeiro.infinitysaga.ui.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.infinitysaga.R;
import com.celsoribeiro.infinitysaga.model.Movie;
import com.celsoribeiro.infinitysaga.ui.presenter.MoviesPresenter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_movie_photo)
    ImageView iv_movie_photo;
    @BindView(R.id.tv_movie_name)
    TextView tv_movie_name;
    @BindView(R.id.yearMovie)
    TextView yearMovie;
    @BindView(R.id.genresMovie)
    TextView genresMovie;
    @BindView(R.id.rateMovie)
    TextView rateMovie;
    @BindView(R.id.is_favorite)
    ImageView is_favorite;

    private final MoviesPresenter presenter;

    public MovieViewHolder(View itemView, MoviesPresenter presenter) {
        super(itemView);
        this.presenter = presenter;
        ButterKnife.bind(this, itemView);
    }

    public void render(Movie movie) {
        hookListeners(movie);
        renderMoviePicture(movie.getPoster());
        renderMovieName(movie.getTitle());
        renderMovieYear(movie.getYear());
        renderMovieGenres(movie.getGenre());
        renderMovieFavorite(movie.getTitle());
        //renderMovieRate(movie.getRated().toString());

    }

    private void hookListeners(final Movie movie) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onMovieClicked(movie);
            }
        });
        is_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onFavoriteClicked(movie);
            }
        });
    }

    private void renderMoviePicture(String photo) {
        Picasso.with(getContext()).load(photo).fit().centerCrop().into(iv_movie_photo);
    }

    private void renderMovieFavorite(String title) {
        List<Movie> favorites = presenter.getFavorites();
        for (Movie movie : favorites) {
            if (movie.getTitle().equals(title)) {
                is_favorite.setImageResource(R.drawable.favorite);
                return;
            }
        }
        is_favorite.setImageResource(R.drawable.unfavorite);
    }

    private void renderMovieName(String name) {
        tv_movie_name.setText(name);
    }

    private void renderMovieYear(String year) {
        yearMovie.setText(year);
    }

    private void renderMovieGenres(String genres) {
        genresMovie.setText(genres);
    }

    private void renderMovieRate(String rate) {
        rateMovie.setText(rate);
    }

    private Context getContext() {
        return itemView.getContext();
    }
}
