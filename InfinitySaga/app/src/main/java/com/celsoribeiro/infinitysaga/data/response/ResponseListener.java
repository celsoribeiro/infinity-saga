package com.celsoribeiro.infinitysaga.data.response;


import com.celsoribeiro.infinitysaga.model.Movie;

import java.util.List;

public interface ResponseListener {

  void onSuccess(List<Movie> movieList);

  void onFailure();

}
