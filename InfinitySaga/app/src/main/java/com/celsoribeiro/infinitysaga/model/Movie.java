package com.celsoribeiro.infinitysaga.model;

import java.io.Serializable;

public class Movie implements Serializable {
    private String title;
    private String year;
    private Rated rated;
    private String released;
    private String runtime;
    private String genre;
    private String director;
    private String writer;
    private String actors;
    private String plot;
    private String poster;
    private boolean favorite;

    public String getTitle() { return title; }

    public void setTitle(String value) { this.title = value; }


    public String getYear() { return year; }

    public void setYear(String value) { this.year = value; }


    public Rated getRated() { return rated; }

    public void setRated(Rated value) { this.rated = value; }


    public String getReleased() { return released; }

    public void setReleased(String value) { this.released = value; }


    public String getRuntime() { return runtime; }

    public void setRuntime(String value) { this.runtime = value; }


    public String getGenre() { return genre; }

    public void setGenre(String value) { this.genre = value; }


    public String getDirector() { return director; }

    public void setDirector(String value) { this.director = value; }


    public String getWriter() { return writer; }

    public void setWriter(String value) { this.writer = value; }


    public String getActors() { return actors; }

    public void setActors(String value) { this.actors = value; }


    public String getPlot() { return plot; }

    public void setPlot(String value) { this.plot = value; }


    public String getPoster() { return poster; }

    public void setPoster(String value) { this.poster = value; }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
