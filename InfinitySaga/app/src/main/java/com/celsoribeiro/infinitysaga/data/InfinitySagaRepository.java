package com.celsoribeiro.infinitysaga.data;
import com.celsoribeiro.infinitysaga.data.response.ResponseListener;

public class InfinitySagaRepository {

    private InfinitySagaRemoteRepository infinitySagaRemoteRepository;

    public InfinitySagaRepository() {

        this.infinitySagaRemoteRepository = new InfinitySagaRemoteRepository();
    }
    public void getAllMovies(final ResponseListener listener) {
        infinitySagaRemoteRepository.getAllMovies(listener);
    }
}


