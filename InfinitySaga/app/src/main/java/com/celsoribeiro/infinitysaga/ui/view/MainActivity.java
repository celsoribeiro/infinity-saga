package com.celsoribeiro.infinitysaga.ui.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.infinitysaga.R;
import com.celsoribeiro.infinitysaga.model.Movie;
import com.celsoribeiro.infinitysaga.ui.presenter.MoviesPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements MoviesPresenter.View {

    MoviesPresenter presenter;
    private MoviesAdapter adapter;

    @BindView(R.id.tv_empty_case)
    View emptyCaseView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.searchView)
    androidx.appcompat.widget.SearchView searchView;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    List<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initializePresenter();
        initializeRecyclerView();
        initializeSearchView();
        showLoading();
        presenter.getAllMovies();

    }

    @Override
    public int getLayoutId() {
        return R.layout.main_activity;
    }

    @Override
    public void openMovieScreen(Movie movie) {
        MovieDetailActivity.open(this, movie);
    }

    @Override
    public void showEmptyCase() {
        emptyCaseView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyCase() {
        emptyCaseView.setVisibility(View.GONE);
    }

    @Override
    public void showMovies(List<Movie> movies) {
        hideLoading();
        bindList(movies);
        this.movies = movies;
    }

    private void initializePresenter() {
        presenter = new MoviesPresenter(this);
        presenter.setView(this);
    }

    private void bindList(List<Movie> movieList) {
        adapter = new MoviesAdapter(movieList, presenter);
        recyclerView.setAdapter(adapter);
    }

    private void initializeRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

    }

    private void initializeSearchView() {
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarTitle.setVisibility(View.GONE);
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                toolbarTitle.setVisibility(View.VISIBLE);
                return false;
            }
        });
        searchView.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    showLoading();
                    presenter.getAllMovies();
                } else {
                    filterList(newText);
                }
                return false;
            }
        });
    }

    @Override
    public void filterList(String text) {
        List<Movie> movies = new ArrayList<>();
        for (Movie movie : this.movies) {
            if (movie.getTitle().toLowerCase().startsWith(text.toLowerCase())) {
                movies.add(movie);
            }
        }
        bindList(movies);
    }

    @Override
    public void refreshList() {
        adapter.notifyDataSetChanged();
    }
}
