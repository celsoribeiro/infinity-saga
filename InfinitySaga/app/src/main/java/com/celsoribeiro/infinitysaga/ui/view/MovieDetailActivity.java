package com.celsoribeiro.infinitysaga.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.celsoribeiro.infinitysaga.R;
import com.celsoribeiro.infinitysaga.model.Movie;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class MovieDetailActivity extends BaseActivity {


    @BindView(R.id.iv_movie_photo)
    ImageView moviePosterImageView;
    @BindView(R.id.tv_movie_name)
    TextView moovieTitleTextView;
    @BindView(R.id.tv_movie_description)
    TextView movieDescriptionTextView;

    Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movie = (Movie) getIntent().getSerializableExtra("Movie");
        bindMovie(movie);
        setTitle(movie.getTitle());
    }

    @Override
    public int getLayoutId() {
        return R.layout.movie_detail_activity;
    }

    public void bindMovie(Movie movie) {
        hideLoading();
        Picasso.with(this).load(movie.getPoster()).fit().centerCrop().into(moviePosterImageView);
        moovieTitleTextView.setText(movie.getDirector());
        movieDescriptionTextView.setText(movie.getPlot());

    }

    public static void open(Context context, Movie movie) {
        Intent intent = new Intent(context, MovieDetailActivity.class);
        intent.putExtra("Movie", movie);
        context.startActivity(intent);
    }


}
