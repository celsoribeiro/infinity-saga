package com.celsoribeiro.infinitysaga.ui.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.celsoribeiro.infinitysaga.data.InfinitySagaRepository;
import com.celsoribeiro.infinitysaga.data.response.ResponseListener;
import com.celsoribeiro.infinitysaga.model.Movie;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MoviesPresenter extends Presenter<MoviesPresenter.View> {

    private final InfinitySagaRepository repository;
    SharedPreferences mPrefs;
    Context context;

    public MoviesPresenter(Context context) {
        this.context = context;
        this.repository = new InfinitySagaRepository();
        this.mPrefs = context.getSharedPreferences("preferenceFileName", 0);
    }

    @Override
    public void setView(View view) {
        super.setView(view);
    }

    public void onMovieClicked(Movie movie) {
        getView().openMovieScreen(movie);
    }

    public void onFavoriteClicked(Movie m) {
        List<Movie> favorites = getFavorites();
        int index = 0;
        for(Movie movie : favorites){
            if(movie.getTitle().equals(m.getTitle())){
                favorites.remove(index);
                updateFavorites(favorites);
                getView().refreshList();
                return;
            }
            index ++;
        }
        favorites.add(m);
        updateFavorites(favorites);
        getView().refreshList();
        Toast.makeText(context,"Add to Favorites",Toast.LENGTH_SHORT).show();

    }

    public void getAllMovies() {
        repository.getAllMovies(new ResponseListener() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                if (movieList != null && movieList.size() > 0) {
                    addMovies(movieList);
                    getView().showMovies(movieList);
                    return;
                }
                getView().hideLoading();
                getView().showEmptyCase();
            }

            @Override
            public void onFailure() {
                List<Movie> movieList = getMovies();
                if (movieList != null && movieList.size() > 0) {
                    getView().showMovies(movieList);
                    return;
                }
                getView().hideLoading();
                getView().showEmptyCase();
            }
        });
    }

    private void addMovies(List<Movie> movieList) {
        SharedPreferences.Editor sharedPreferencesEditor = mPrefs.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(movieList);
        sharedPreferencesEditor.putString("movieList", serializedObject);
        sharedPreferencesEditor.apply();
    }

    private List<Movie> getMovies() {
        if (mPrefs.contains("movieList")) {
            final Gson gson = new Gson();
            Type type = new TypeToken<List<Movie>>() {
            }.getType();
            return gson.fromJson(mPrefs.getString("movieList", ""), type);
        }
        return null;
    }
    public void addFavorites(Movie movie) {
        SharedPreferences.Editor sharedPreferencesEditor = mPrefs.edit();
        Type type = new TypeToken<List<Movie>>() {
        }.getType();
        final Gson gson = new Gson();
        List<Movie> movieList = gson.fromJson(mPrefs.getString("favorites", ""), type);
        movieList.add(movie);
        String serializedObject = gson.toJson(movieList);
        sharedPreferencesEditor.putString("favorites", serializedObject);
        sharedPreferencesEditor.apply();
    }
    public void updateFavorites(List<Movie> movieList) {
        SharedPreferences.Editor sharedPreferencesEditor = mPrefs.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(movieList);
        sharedPreferencesEditor.putString("favorites", serializedObject);
        sharedPreferencesEditor.apply();
    }
    public List<Movie> getFavorites() {
        if (mPrefs.contains("favorites")) {
            final Gson gson = new Gson();
            Type type = new TypeToken<List<Movie>>() {
            }.getType();
            return gson.fromJson(mPrefs.getString("favorites", ""), type);
        }
        return new ArrayList<Movie>();
    }
    public interface View extends Presenter.View {

        void showEmptyCase();

        void hideEmptyCase();

        void showMovies(List<Movie> movies);

        void openMovieScreen(Movie movie);

        void filterList(String text);

        void refreshList();

    }
}
