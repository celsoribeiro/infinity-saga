package com.celsoribeiro.infinitysaga.ui.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import com.celsoribeiro.infinitysaga.R;
import com.celsoribeiro.infinitysaga.ui.presenter.Presenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements Presenter.View {

  @Nullable @BindView(R.id.toolbar)
  Toolbar toolbar;
  @Nullable
  @BindView(R.id.progress_bar) View loadingView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutId());
    initializeButterKnife();
    initializeToolbar();
  }

  public abstract int getLayoutId();

  @Override public void showLoading() {
    if (loadingView != null) {
      loadingView.setVisibility(View.VISIBLE);
    }
  }

  @Override public void hideLoading() {
    if (loadingView != null) {
      loadingView.setVisibility(View.GONE);
    }
  }

  private void initializeButterKnife() {
    ButterKnife.bind(this);
  }

  protected void initializeToolbar() {
    if (toolbar != null) {
      setSupportActionBar(toolbar);
    }
  }
}
